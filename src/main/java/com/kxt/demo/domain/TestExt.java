package com.kxt.demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName test_ext
 */
@TableName(value ="test_ext")
@Data
public class TestExt implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * tid
     */
    private Integer tid;

    /**
     * 得分
     */
    private Integer score;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}