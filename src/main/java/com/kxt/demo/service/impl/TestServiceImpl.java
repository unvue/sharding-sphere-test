package com.kxt.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kxt.demo.domain.Test;
import com.kxt.demo.domain.TestExt;
import com.kxt.demo.mapper.TestMapper;
import com.kxt.demo.service.TestExtService;
import com.kxt.demo.service.TestService;
import org.apache.shardingsphere.infra.hint.HintManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangbo
 * @description 针对表【test】的数据库操作Service实现
 * @createDate 2024-02-05 17:52:51
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test>
        implements TestService {

    @Resource
    private TestExtService testExtServiceImpl;

    @Override
    public Test saveTest(Test test) {
        HintManager hint = HintManager.getInstance();
        hint.setWriteRouteOnly();
        int num = baseMapper.insert(test);
        Test res = null;
        if (num > 0) {
            res = baseMapper.selectById(test.getId());
        }

        TestExt ext = new TestExt();
        ext.setTid(res.getId());
        ext.setScore(test.getScore());
        testExtServiceImpl.save(ext);

        hint.clearShardingValues();
        hint.close();
        return res;
    }

    @Override
    public List<Test> getExtList(Test test) {
        return baseMapper.getExtList(test);
    }
}




