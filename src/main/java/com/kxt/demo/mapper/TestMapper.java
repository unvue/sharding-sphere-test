package com.kxt.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kxt.demo.domain.Test;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhangbo
 * @description 针对表【test】的数据库操作Mapper
 * @createDate 2024-02-05 17:52:51
 * @Entity com.kxt.shardingjdbc.domain.Test
 */
public interface TestMapper extends BaseMapper<Test> {

    List<Test> getExtList(@Param("test") Test test);
}




