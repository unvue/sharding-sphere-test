package com.kxt.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kxt.demo.domain.TestExt;
import com.kxt.demo.service.TestExtService;
import com.kxt.demo.mapper.TestExtMapper;
import org.springframework.stereotype.Service;

/**
* @author zhangbo
* @description 针对表【test_ext】的数据库操作Service实现
* @createDate 2024-02-07 11:17:27
*/
@Service
public class TestExtServiceImpl extends ServiceImpl<TestExtMapper, TestExt>
    implements TestExtService{

}




