## Sharding sphere 读写分离案例

### Maven 包
```xml


<properties>
    <java.version>1.8</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <spring-boot.version>2.7.6</spring-boot.version>
    <mybatis-plus.version>3.5.5</mybatis-plus.version>
    <mybatis-pagehelper.version>1.4.5</mybatis-pagehelper.version>
    <druid.version>1.2.21</druid.version>
    <shardingsphere.version>5.2.1</shardingsphere.version>
</properties>

<dependencies>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <!-- mybatis-plus -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>${mybatis-plus.version}</version>
    </dependency>
    <!-- 分页 -->
    <dependency>
        <groupId>com.github.pagehelper</groupId>
        <artifactId>pagehelper-spring-boot-starter</artifactId>
        <version>${mybatis-pagehelper.version}</version>
    </dependency>
    <!-- Mysql Connector -->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.33</version>
    </dependency>
    <!-- sharding-sphere 5.2.1 -->
    <dependency>
        <groupId>org.apache.shardingsphere</groupId>
        <artifactId>shardingsphere-jdbc-core-spring-boot-starter</artifactId>
        <version>${shardingsphere.version}</version>
    </dependency>
    <!-- sharding-sphere 5.2.1必须要，不要用spring-boot版本的 -->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>${druid.version}</version>
    </dependency>
    <!-- sharding-sphere 5.2.1必须要 -->
    <dependency>
        <groupId>org.yaml</groupId>
        <artifactId>snakeyaml</artifactId>
        <version>1.33</version>
    </dependency>
</dependencies>
```
注意，`snakeyaml`必须引入，`druid`不要用spring-boot版本

### 强制主库路由查询
```java
package com.kxt.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kxt.demo.domain.Test;
import com.kxt.demo.domain.TestExt;
import com.kxt.demo.mapper.TestMapper;
import com.kxt.demo.service.TestExtService;
import com.kxt.demo.service.TestService;
import org.apache.shardingsphere.infra.hint.HintManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhangbo
 * @description 针对表【test】的数据库操作Service实现
 * @createDate 2024-02-05 17:52:51
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test>
        implements TestService {

    @Resource
    private TestExtService testExtServiceImpl;

    @Override
    public Test saveTest(Test test) {
        HintManager hint = HintManager.getInstance();
        hint.setWriteRouteOnly();
        int num = baseMapper.insert(test);
        Test res = null;
        if (num > 0) {
            res = baseMapper.selectById(test.getId());
        }

        TestExt ext = new TestExt();
        ext.setTid(res.getId());
        ext.setScore(test.getScore());
        testExtServiceImpl.save(ext);

        hint.clearShardingValues();
        hint.close();
        return res;
    }

    @Override
    public List<Test> getExtList(Test test) {
        return baseMapper.getExtList(test);
    }
}
```
```java
// 以下代码设置了...部分强制走写库路由

HintManager hint = HintManager.getInstance();
hint.setWriteRouteOnly();

...

hint.clearShardingValues();
hint.close();

```

### 配置
```yaml
server:
  port: 9090
spring:
  shardingsphere:
    props:
      # 显示sql转换日志
      sql-show: true
    datasource:
      # 数据源名称
      names: master,slave1,slave2
      # master数据源配置
      master:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.0.147:3388/sd_test?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=Asia/Shanghai
        username: root
        password: kxtsoft2010
        initial-size: 5
        min-idle: 5
        maxActive: 20
        maxWait: 60000
        timeBetweenEvictionRunsMillis: 60000
        minEvictableIdleTimeMillis: 300000
        validationQuery: SELECT 1 FROM DUAL
        testWhileIdle: true
        testOnBorrow: false
        testOnReturn: false
        poolPreparedStatements: true
        maxPoolPreparedStatementPerConnectionSize: 20
        connectionProperties: druid.stat.mergeSql\=true;druid.stat.slowSqlMillis\=5000
      # slave1数据源配置
      slave1:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.0.149:3388/sd_test?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=Asia/Shanghai
        username: root
        password: kxtsoft2010
        initial-size: 5
        min-idle: 5
        maxActive: 20
        maxWait: 60000
        timeBetweenEvictionRunsMillis: 60000
        minEvictableIdleTimeMillis: 300000
        validationQuery: SELECT 1 FROM DUAL
        testWhileIdle: true
        testOnBorrow: false
        testOnReturn: false
        poolPreparedStatements: true
        maxPoolPreparedStatementPerConnectionSize: 20
        connectionProperties: druid.stat.mergeSql\=true;druid.stat.slowSqlMillis\=5000
      # slave2数据源配置
      slave2:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.0.150:3388/sd_test?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=Asia/Shanghai
        username: root
        password: kxtsoft2010
        initial-size: 5
        min-idle: 5
        maxActive: 20
        maxWait: 60000
        timeBetweenEvictionRunsMillis: 60000
        minEvictableIdleTimeMillis: 300000
        validationQuery: SELECT 1 FROM DUAL
        testWhileIdle: true
        testOnBorrow: false
        testOnReturn: false
        poolPreparedStatements: true
        maxPoolPreparedStatementPerConnectionSize: 20
        connectionProperties: druid.stat.mergeSql\=true;druid.stat.slowSqlMillis\=5000
    # 规则配置
    rules:
      # 读写分离
      readwrite-splitting:
        data-sources:
          # 自定义的数据源配置
          myds:
            # 静态策略
            static-strategy:
              # 写数据源名称
              write-data-source-name: master
              # 读数据源名称
              read-data-source-names:
                - slave1
                - slave2
            #负载均衡器，指定下面的自定义负载均衡器名称 
            load-balancer-name: round-robin
        # 负载均衡器
        load-balancers:
          # 自定义负载均衡器名称
          round-robin:
            # 负载均衡器类型：轮询
            type: ROUND_ROBIN
```
