package com.kxt.demo.mapper;

import com.kxt.demo.domain.TestExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zhangbo
* @description 针对表【test_ext】的数据库操作Mapper
* @createDate 2024-02-07 11:17:27
* @Entity com.kxt.demo.domain.TestExt
*/
public interface TestExtMapper extends BaseMapper<TestExt> {

}




