package com.kxt.demo.controller;

import com.kxt.demo.domain.Test;
import com.kxt.demo.service.TestService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private TestService testServiceImpl;

    @GetMapping("/list")
    public List<Test> list() {
        return testServiceImpl.list();
    }


    @GetMapping("/ext-list")
    public List<Test> extList(Test test) {
        return testServiceImpl.getExtList(test);
    }

    @PostMapping("/save")
    public Test save(@RequestBody Test test) {
        return testServiceImpl.saveTest(test);
    }
}
