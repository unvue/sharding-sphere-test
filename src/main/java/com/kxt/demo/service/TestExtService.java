package com.kxt.demo.service;

import com.kxt.demo.domain.TestExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author zhangbo
* @description 针对表【test_ext】的数据库操作Service
* @createDate 2024-02-07 11:17:27
*/
public interface TestExtService extends IService<TestExt> {

}
