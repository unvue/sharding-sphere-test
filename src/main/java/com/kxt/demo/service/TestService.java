package com.kxt.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kxt.demo.domain.Test;

import java.util.List;

/**
* @author zhangbo
* @description 针对表【test】的数据库操作Service
* @createDate 2024-02-05 17:52:51
*/
public interface TestService extends IService<Test> {

    Test saveTest(Test test);

    List<Test> getExtList(Test test);
}
